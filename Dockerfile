# Use an official ubuntu as a parent image
FROM debian
LABEL maintainer = "lopdal84@gmail.com"

# Set the working directory
WORKDIR /root/work

#Update and install depencys from pacakge manager
RUN apt-get update && apt-get install -y \
	git \
	libcurl4-openssl-dev \
	curl \
	python-setuptools \
	python \
 	python-pip \
	zlib1g-dev \
	locate \
	nmap \
	ruby-dev \
	wget

# Copy the current directory contents into the container at WORKDIR and run setup script
COPY setup-gauntlt-env.sh /root/work/setup-gauntlt-env.sh
RUN sh setup-gauntlt-env.sh

#Path of arachni binary
ENV PATH="/root/work/arachni-1.5.1-0.5.12/bin:${PATH}"

#Changing WORKDIR
WORKDIR /root/work/gauntlt-fork

#Clean and run install script 
RUN sed -i 's/sudo //g' install_gauntlt_deps.sh 
RUN /bin/bash -c "source ./install_gauntlt_deps.sh"

#Path of Heartbleed binary
ENV PATH="/root/go/bin:${PATH}"

#Test if gauntlt is ready for use
RUN DIRB_WORDLISTS=`locate dirb | grep "/dirb/wordlists$"` SSLYZE_PATH=`which sslyze` SQLMAP_PATH=`which sqlmap` bash ./ready_to_rumble.sh

#Check for updates in gauntlt-fork (get new security-test)
RUN git pull

#Start up localhost and run gauntlt
CMD cd vendor/gruyere && bash ./manual_launch.sh && cd ../.. && DIRB_WORDLISTS=`locate dirb | grep "/dirb/wordlists$"` SSLYZE_PATH=`which sslyze` SQLMAP_PATH=`which sqlmap` gauntlt --format=html --outfile=result.html
