## Continuous Security med​ gauntlt 
Dette repo kan klones for å få bruke et Docker image til å kjøre [gauntlt](http://gauntlt.org/) i
Jenkins Pipeline​ eller som standalone/ sandbox. 

For å lage egendefinert test, må dette repo klones:
https://github.com/leo100584/gauntlt-fork/

Tester må skreves her:
https://github.com/leo100584/gauntlt-fork/tree/master/examples


#### Build docker image on host:
```
docker build --tag=debian-gauntlt .
```
##### Run docker container from host:
```
docker run -t --name auto-pen-test<number> debian-gauntlt
```
##### Copy result from container (back to the host):
```
docker cp auto-pen-test<number>:/root/work/gauntlt-fork/result<test-name>.html /root/result<test-name>.html
```
##### Copy back from root to local-user directory:
```
cp /root/result<test-name>.html /home/<user-name>
```
