#!/bin/bash

#Download arachni binary
wget https://github.com/Arachni/arachni/releases/download/v1.5.1/arachni-1.5.1-0.5.12-linux-x86_64.tar.gz
tar -xvf arachni-1.5.1-0.5.12-linux-x86_64.tar.gz

#Download and checkout the gauntlt-fork with own user defeined security tests
git clone https://github.com/leo100584/gauntlt-fork/
cd gauntlt-fork

#Update rake (ruby package manager)
gem install rake

#install rvm
gem install rvm

#Installing gauntlt with ruby package manager
gem install gauntlt
